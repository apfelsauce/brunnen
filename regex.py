#/usr/bin/env python

import re

sceneHeading = re.compile("^(EXT|INT|EST|\.[^\.]).*$", re.IGNORECASE)

characterName = re.compile("^([^\s])([A-Z ':\s\d]+)(\(.*\))?:?\^?$")

emptyLine = re.compile("^\s*$")

emptyCont = re.compile("^\s\s$")

transition = re.compile(".*TO:$|^\>[^\<]*$")

centered = re.compile("^\>[^<]*\<$")

underlined = re.compile("^\_[^\_$]*\_$")

italics = re.compile("^\*[^\*]+\*$")

note = re.compile("\[\[[\w\s\d]*\]\]")

noteStart = re.compile("\[\[.*$")

noteEnd = re.compile("^.*\]\]")

forceAction = re.compile("^!")

filename = re.compile(".fountain$")
