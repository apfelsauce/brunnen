#created by Benedikt Gerhard
#released "as is", with no waranty towards any functionality
#created in python2, using gtk gui
#this will be a fountain screenwriting editor.
#for more information on fountain visit: fountain.io


import pygtk
pygtk.require("2.0")
import gtk
import pango
import regex as rx
import analyst
import scenify


class Window:
    scrollAdj = None
    text = "\n"
    isSaved = False
    savepath = ""
    
    scenes = scenify.script()
    
    
    
    #-------define all the callbackfunctions-------#
    def close(self, widget=None, event=None, data=None):
        gtk.main_quit()
        return False
    
    #-------function to check wether cursor is on screen
    def cursOnScreen(self, viewR, cursR):
        if cursR[0] >= viewR[0] and cursR[0] <= viewR[0]+viewR[2]:
            if cursR[1] >= viewR[1] and cursR[1] <= viewR[1]+viewR[3]:
                return True
        return False
    
    def setTitle(self):
        fileName = self.savepath.split("/")[-1]
        self.window.set_title("brunnen -- "+str(fileName))
    #-------open files:
    def load(self, widget):
        self.FileChooser = gtk.FileChooserDialog(title = "open file", action = gtk.FILE_CHOOSER_ACTION_OPEN, buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                    gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        response = self.FileChooser.run()
        fileName = self.FileChooser.get_filename()
        self.FileChooser.destroy()
        #make sure the filechooser has responded with "Ok"
        if fileName != None and response == -3:
            self.isSaved = True
            #remove any fileextensions
            savepath = fileName.split(".")[:-1]
            if len(savepath)<1:
                savepath = fileName
            self.savepath = ""
            for i in savepath:
                self.savepath += i
            
            
            f = open(fileName, "r")
            self.text = f.read()
            self.tagify()
            self.setTitle()
            
    #-------clear:
    def clear(self, widget=None):
        self.isSaved = False
        self.text = ""
        self.savepath = ""
        self.tagify()
        self.setTitle()
    
    #-------save new:
    def save_new(self, widget = None):
        self.isSaved = False
        self.save()
    
    #-------save files:
    def save(self, widget=None):
        if not self.isSaved:
            self.FileChooser = gtk.FileChooserDialog(title = "save file", action = gtk.FILE_CHOOSER_ACTION_SAVE, buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                    gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
            response = self.FileChooser.run()
            fileName = self.FileChooser.get_filename()
            self.FileChooser.destroy()
            if fileName != None:
                self.isSaved = True
                #remove any fileextensions
                fileName = fileName.split(".")[0]
                self.savepath = fileName
                self.save()
                self.setTitle()
        else:
            #this is where the programme wants yo fileextensions. this is probably stupid. It means you can't save anything else than .fountain files.
            f = open(self.savepath+ ".fountain", "w+")
            f.write(self.text)
    
        
        
    
    #-------edit text:
    def textChanged(self, widget):
        self.text = self.TextBuffer.get_text(self.TextBuffer.get_start_iter(),self.TextBuffer.get_end_iter())
        cursPos = self.TextBuffer.get_property("cursor-position")
        self.tagify(cursPos)
    
    #-------function to work-arround the text-buffer not accepting a list of iters
    def textbufferinsertshortcut(self, textIter,text, tags):
        if len(tags) == 1:
            self.TextBuffer.insert_with_tags_by_name(textIter, text, "generalTag", tags[0])
        if len(tags) == 2:
            self.TextBuffer.insert_with_tags_by_name(textIter, text, "generalTag", tags[0], tags[1])
        if len(tags) == 3:
            self.TextBuffer.insert_with_tags_by_name(textIter, text, "generalTag", tags[0], tags[1], tags[2])
        if len(tags) == 4:
            self.TextBuffer.insert_with_tags_by_name(textIter, text, "generalTag", tags[0], tags[1], tags[2], tags[3])
    
    
    
    #this gets called by analyst's analyse function
    def printline(self, line, linetype, addtags, last):
        linestart = self.TextBuffer.get_iter_at_line(-1)
        self.textbufferinsertshortcut(linestart,line,[linetype]+addtags)
        lineend = self.TextBuffer.get_end_iter()
        for tag in addtags:
            self.TextBuffer.apply_tag_by_name(tag, linestart, lineend)
        if not last:
            self.TextBuffer.insert(lineend, "\n")
            
    #-------the tagify function that uses analyst.py
    def tagify(self,cursPos = 0):
        self.TextBuffer.set_text("")
        analyst.analyseText(self.text, self)
        cursor = self.TextBuffer.get_iter_at_offset(cursPos)
        self.TextBuffer.place_cursor(cursor)
        if self.scenes.changed():
            self.onSceneChange()
    
    
    tmpItrs = []
    
    #when Scenes change: change the scenes seen in the sideview.
    def onSceneChange(self):
        scenLib = self.scenes.scenes
        
        #because of missing "remove all" functionality and iters changing after stuff in front being deleted, this is how stuff is deleted in the sceneList now.
        self.sceneList.foreach(self.deleteList)
        for itr in self.tmpItrs:
            self.sceneList.remove(itr)
        for scene in scenLib:
            try:
                self.sceneList.append([scene["loc"]])
            except KeyError:
                print(scene)
    
    #because of missing "remove all" functionality and iters changing after stuff in front being deleted, this is how stuff is deleted in the sceneList now.
    def deleteList(self, model, path, itr, usr_data = None):
        self.tmpItrs = [itr] + self.tmpItrs
        
    
    def jumpToScene(self, widget,row,col):
        scene = self.scenes.scenes[row[0]]
        line = scene["lin"]
        lineIter = self.TextBuffer.get_iter_at_line(line)
        self.TextBuffer.place_cursor(lineIter)
        self.window.set_focus(self.ScrollWindow)
        self.TextBuffer.move_mark(self.cursMark, lineIter)
        self.TextView.scroll_mark_onscreen(self.cursMark)
        
    
    #------do keyboard shortcuts:
    def keyPress(self, widget, event):
        keyval = event.keyval
        keyName = gtk.gdk.keyval_name(keyval)
        state = event.state
        ctrl = (state & gtk.gdk.CONTROL_MASK)
        if ctrl and keyName == "s":
            self.save()
    
    
    #------this is where the window gets defined------#
    def __init__(self):
        
        #create the window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.close)
        self.window.set_title("brunnen")
        self.window.set_icon_from_file("icon.png")
        self.window.set_geometry_hints(min_width = 500, min_height = 500)
        self.window.connect("key-press-event", self.keyPress)
        
        #create boxes to pack stuff in
        self.mainBox = gtk.VBox(False, 0)
        self.window.add(self.mainBox)
        self.optionBox = gtk.HBox(False, 0)
        self.mainBox.pack_start(self.optionBox, False, False)
        self.divider = gtk.HSeparator()
        self.mainBox.pack_start(self.divider, False, False)
        self.contBox = gtk.HBox(False,0)
        self.mainBox.pack_start(self.contBox, True, True)
        
        #create the save and open buttons
        self.saveBtn = gtk.Button("save")
        self.saveBtn.connect("clicked", self.save)
        self.optionBox.pack_start(self.saveBtn, False, False)
        self.saveNewBtn = gtk.Button("save new")
        self.saveNewBtn.connect("clicked", self.save_new)
        self.optionBox.pack_start(self.saveNewBtn, False, False)
        self.loadBtn = gtk.Button("open")
        self.loadBtn.connect("clicked", self.load)
        self.optionBox.pack_start(self.loadBtn, False, False)
        self.clearBtn = gtk.Button("clear")
        self.clearBtn.connect("clicked", self.clear)
        self.optionBox.pack_start(self.clearBtn, False, False)
        
        
        #create the text-elements
        
        #create the text-tags
        self.TagTable = gtk.TextTagTable()
        
        self.generalTag = gtk.TextTag(name = "generalTag")
        self.generalTag.set_property("left-margin-set", True)
        self.generalTag.set_property("left-margin", 10)
        self.generalTag.set_property("family-set", True)
        self.generalTag.set_property("family", "Palatino Linotype")
        self.generalTag.set_property("size-set", True)
        self.generalTag.set_property("size-points", 18)
        self.TagTable.add(self.generalTag)
        
        self.sceneHeader = gtk.TextTag(name = "sceneHeading")
        self.sceneHeader.set_property("underline-set", True)
        self.sceneHeader.set_property("underline", pango.UNDERLINE_SINGLE)
        self.TagTable.add(self.sceneHeader)
        
        self.centered = gtk.TextTag(name = "centered")
        self.centered.set_property("justification-set", True)
        self.centered.set_property("justification", gtk.JUSTIFY_CENTER)
        self.TagTable.add(self.centered)
        
        self.character = gtk.TextTag(name = "characterName")
        self.character.set_property("left-margin-set", True)
        self.character.set_property("left-margin", 200)
        self.TagTable.add(self.character)
        
        self.speech = gtk.TextTag(name = "speech")
        self.speech.set_property("left-margin-set", True)
        self.speech.set_property("left-margin", 100)
        self.speech.set_property("right-margin-set", True)
        self.speech.set_property("right-margin", 100)
        self.TagTable.add(self.speech)
        
        self.action = gtk.TextTag(name = "action")
        self.TagTable.add(self.action)
        
        self.transition = gtk.TextTag(name = "transition")
        self.transition.set_property("justification-set", True)
        self.transition.set_property("justification", gtk.JUSTIFY_RIGHT)
        self.transition.set_property("right-margin-set", True)
        self.transition.set_property("right-margin", 50)
        self.TagTable.add(self.transition)
        
        self.underlined = gtk.TextTag(name = "underlined")
        self.underlined.set_property("underline-set", True)
        self.underlined.set_property("underline", pango.UNDERLINE_SINGLE)
        self.TagTable.add(self.underlined)
        
        self.italic = gtk.TextTag(name = "italics")
        self.italic.set_property("style-set", True)
        self.italic.set_property("style", pango.STYLE_ITALIC)
        self.TagTable.add(self.italic)
        
        self.bold = gtk.TextTag(name = "bold")
        self.bold.set_property("style-set", True)
        self.bold.set_property("style", pango.STYLE_OBLIQUE)
        self.TagTable.add(self.bold)
        
        
        #create the sidepanel that shows the scenes and stuff
        self.sceneList = gtk.ListStore(str)     #this is a list store modell, maybe make it into a tree?
        self.sceneView = gtk.TreeView()         #this is the tree-view for the sidebar.
        self.sceneView.set_model(self.sceneList)
        
        self.rendererText = gtk.CellRendererText()
        self.column = gtk.TreeViewColumn("Scenes or stuff", self.rendererText, text = 0)
        self.sceneView.append_column(self.column)
        self.sceneView.connect("row-activated", self.jumpToScene)
        
        
                
        #create the view and its buffer and the scroll-window
        self.TextBuffer = gtk.TextBuffer(table = self.TagTable)
        self.TextBuffer.set_text(self.text)
        self.cursMark = self.TextBuffer.create_mark("cursor", self.TextBuffer.get_start_iter(), True)
        self.TextBuffer.connect("end-user-action", self.textChanged)
        self.TextView = gtk.TextView()
        self.TextView.set_buffer(self.TextBuffer)
        self.TextView.set_property("wrap-mode", gtk.WRAP_WORD)
        
        
        
        #create the scroll-window for the treeview. Use add for textview and treeview, anything else: use add_with_viewport.
        self.ScrollWindowT = gtk.ScrolledWindow()
        self.ScrollWindowT.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.ScrollWindowT.add(self.sceneView)
        self.scrollAdjT = self.ScrollWindowT.get_vadjustment()
        self.contBox.pack_start(self.ScrollWindowT, True, True)
        
        
        self.divider2 = gtk.VSeparator()
        self.contBox.pack_start(self.divider2, False, False)
        
        
        #create the scroll-window for the textview. Use add for textview and treeview, anything else: use add_with_viewport.
        self.ScrollWindow = gtk.ScrolledWindow()
        self.ScrollWindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.ScrollWindow.add(self.TextView)
        self.scrollAdj = self.ScrollWindow.get_vadjustment()
        self.contBox.pack_start(self.ScrollWindow, True, True)
        
        #show stuff!
        self.mainBox.show()
        self.optionBox.show()
        self.divider.show()
        self.divider2.show()
        self.contBox.show()
        self.saveBtn.show()
        self.saveNewBtn.show()
        self.loadBtn.show()
        self.clearBtn.show()
        self.sceneView.show()
        self.TextView.show()
        self.ScrollWindowT.show()
        self.ScrollWindow.show()
        #show window last, so that if anything takes longer, everything still appears at once
        self.window.show()
    def main(self):
        gtk.main()

if __name__ == "__main__":
    window = Window()
    window.main()
