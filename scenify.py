#!/usr/bin/env python

class script:
    scenes = []
    
    oldscenes = []
    
    def sceneHeading(self, line, lineNumber):
        #get the scene Number, if none is definded, make one
        try:
            sceneNumber = line.split("#")[1]
        except IndexError:
            sceneNumber = len(self.scenes) +1
        #get the scene name - including ext/int
        sceneDescriptor = line.split("#")[0]
        #get the Location (everything except for the DAY/NIGHT and any "angles")
        sceneLocation = sceneDescriptor.split("-")[0].split(",")[0]
        #try and get any scene angles, seperated with ","
        try:
            sceneAngle = sceneDescriptor.split(",")[1]
        except IndexError:
            sceneAngle = "generic"
        #print(type(self.scenes))
        self.scenes +=[{"loc":sceneLocation,"ang":sceneAngle,"num":sceneNumber,"lin":lineNumber}]
    
    def changed(self):
        return not self.oldscenes == self.scenes
    
    def reset(self):
        self.oldscenes = self.scenes
        self.scenes = []
    
    def printScenes(self):
        print(self.scenes)
