brunnen is a fountain editor.

brunnen brings with it several features specifically for fountain, so your screenplay is not just a plaintext file while writing.
It also has a scene-list, so you can quickly jump between different scenes.

Requirements:
python (2.7) and pygtk