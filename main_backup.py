#created by Benedikt Gerhard
#released "as is", with no waranty towards any functionality
#created in python2, using gtk gui
#this will be a fountain screenwriting editor.
#for more information on fountain visit: fountain.io

#THIS VERSION OF THE PROGRAMME IS ONLY INTENDED AS BACKUP, AS IT CONTAINS OLDER VERSIONS OF SOME CODE.#

import pygtk
pygtk.require("2.0")
import gtk
import pango
import regex as rx
import analyst


class Window:
    scrollAdj = None
    text = "\n"
    isSaved = False
    savepath = ""
    
    scenes = list()
    
    
    
    #-------define all the callbackfunctions-------#
    def close(self, widget=None, event=None, data=None):
        gtk.main_quit()
        return False
    
    #-------function to check wether cursor is on screen
    def cursOnScreen(self, viewR, cursR):
        if cursR[0] >= viewR[0] and cursR[0] <= viewR[0]+viewR[2]:
            if cursR[1] >= viewR[1] and cursR[1] <= viewR[1]+viewR[3]:
                return True
        return False
    
    def setTitle(self):
        fileName = self.savepath.split("/")[-1]
        self.window.set_title("brunnen -- "+str(fileName))
    #-------open files:
    def load(self, widget):
        self.FileChooser = gtk.FileChooserDialog(title = "open file", action = gtk.FILE_CHOOSER_ACTION_OPEN, buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                    gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        response = self.FileChooser.run()
        fileName = self.FileChooser.get_filename()
        self.FileChooser.destroy()
        if fileName != None:
            self.isSaved = True
            self.savepath = fileName
            f = open(fileName, "r")
            self.text = f.read()
            self.tagify()
            self.setTitle()
            
    
    #-------save new:
    def save_new(self, widget = None):
        self.isSaved = False
        self.save()
    
    #-------save files:
    def save(self, widget=None):
        if not self.isSaved:
            self.FileChooser = gtk.FileChooserDialog(title = "save file", action = gtk.FILE_CHOOSER_ACTION_SAVE, buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                    gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
            response = self.FileChooser.run()
            fileName = self.FileChooser.get_filename()
            self.FileChooser.destroy()
            if fileName != None:
                self.isSaved = True
                self.savepath = fileName
                self.save()
                self.setTitle()
        else:
            f = open(self.savepath, "w+")
            f.write(self.text)
    
    #-------other stuff for additional functionality!
    
    def sceneHeading(self, line):
        #get the scene Number, if none is definded, make one
        try:
            sceneNumber = line.split("#")[1]
        except IndexError:
            sceneNumber = len(self.scenes) +1
        #get the scene name - including ext/int
        sceneDescriptor = line.split("#")[0]
        #get the Location (everything except for the DAY/NIGHT and any "angles")
        sceneLocation = sceneDescriptor.split("-")[0].split(",")[0]
        #try and get any scene angles, seperated with ","
        try:
            sceneAngle = sceneDescriptor.split(",")[1]
        except IndexError:
            sceneAngle = "generic"
        #print(type(self.scenes))
        self.scenes +=[{"loc":sceneLocation,"ang":sceneAngle,"num":sceneNumber}]
        
        
    
    #-------edit text:
    def textChanged(self, widget):
        self.text = self.TextBuffer.get_text(self.TextBuffer.get_start_iter(),self.TextBuffer.get_end_iter())
        cursPos = self.TextBuffer.get_property("cursor-position")
        self.tagify(cursPos)
    
    def printline(self, line, linetype, addtags, last):
        linestart = self.TextBuffer.get_end_iter()
        self.TextBuffer.insert_with_tags_by_name(linestart, line, ["generalTag", linetype]+addtags)
        lineend = self.TextBuffer.get_end_iter()
        for tag in addtags:
            self.TextBuffer.apply_tag_by_name(tag, linestart, lineend)
        if not last:
            self.TextBuffer.insert(lineend, "\n")
            
    #the tagify function that uses analyst.py
    def tagify(self,cursPos = 0):
        self.TextBuffer.set_text("")
        analyst.analyse(self.text, self)
        cursor = self.TextBuffer.get_iter_at_offset(cursPos)
        self.TextBuffer.place_cursor(cursor)
    
    def tagifyOld(self,cursPos=0):
        old_scenes = self.scenes
        self.scenes = []
        vadj = self.ScrollWindow.get_vadjustment()
        text = self.text
        lines = text.split("\n")
        self.TextBuffer.set_text("")
        speech = False
        for i in range(0, len(lines)):
            line = lines[i]
            tag = self.action
            if len(line) >= 2:
                if line[0] == "!":
                    tag = self.action
                    speech = False
                elif line[0] == "." and line[1] != ".":
                    tag = self.sceneHeader
                    self.sceneHeading(line)
                    speech = False
                elif rx.sceneHeading.match(line):
                    tag = self.sceneHeader
                    self.sceneHeading(line)
                    speech = False
                elif rx.centered.match(line):
                    tag = self.centered
                elif rx.transition.match(line):
                    tag = self.transition
                    speech = False
                elif rx.characterName.match(line):
                    tag = self.character
                    speech = True
                elif speech:
                    if rx.emptyLine.match(line):
                        if not line == "  ":
                            speech = False
                    else:
                        tag = self.speech
            else:
                speech = False
            if i < len(lines)-1:
                self.TextBuffer.insert_with_tags(self.TextBuffer.get_end_iter(), line + "\n", self.generalTag, tag)
            else:
                self.TextBuffer.insert_with_tags(self.TextBuffer.get_end_iter(), line, self.generalTag, tag)
        #get the cursor-iter and place the cursor where it's supposed to be
        cursor = self.TextBuffer.get_iter_at_offset(cursPos)
        self.TextBuffer.place_cursor(cursor)
        #set the cursor-mark correctly and make the text-view scroll
        self.TextBuffer.move_mark(self.cursMark, cursor)
        #self.ScrollWindow.set_vadjustment(vadj)
        #if not self.cursOnScreen(self.TextView.get_visible_rect(), self.TextView.get_iter_location(cursor)):
        #    self.TextView.scroll_mark_onscreen(self.cursMark)
        
        
        #check wether scenes changed, to save time if nothing has to be updated.
        if old_scenes != self.scenes:
            print("Scene change detected")
        #print(self.scenes)

        
                
    #------do keyboard shortcuts:
    def keyPress(self, widget, event):
        keyval = event.keyval
        keyName = gtk.gdk.keyval_name(keyval)
        state = event.state
        ctrl = (state & gtk.gdk.CONTROL_MASK)
        if ctrl and keyName == "s":
            self.save()
    
    
    #------this is where the window gets defined------#
    def __init__(self):
        
        #create the window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.close)
        self.window.set_title("brunnen")
        self.window.set_icon_from_file("icon.png")
        self.window.set_geometry_hints(min_width = 500, min_height = 500)
        self.window.connect("key-press-event", self.keyPress)
        
        #create boxes to pack stuff in
        self.mainBox = gtk.VBox(False, 0)
        self.window.add(self.mainBox)
        self.optionBox = gtk.HBox(False, 0)
        self.mainBox.pack_start(self.optionBox, False, False)
        self.divider = gtk.HSeparator()
        self.mainBox.pack_start(self.divider, False, False)
        
        #create the save and open buttons
        self.saveBtn = gtk.Button("save")
        self.saveBtn.connect("clicked", self.save)
        self.optionBox.pack_start(self.saveBtn, False, False)
        self.saveNewBtn = gtk.Button("save new")
        self.saveNewBtn.connect("clicked", self.save_new)
        self.optionBox.pack_start(self.saveNewBtn, False, False)
        self.loadBtn = gtk.Button("open")
        self.loadBtn.connect("clicked", self.load)
        self.optionBox.pack_start(self.loadBtn, False, False)
        
        
        #create the text-elements
        
        #create the text-tags
        self.TagTable = gtk.TextTagTable()
        
        self.generalTag = gtk.TextTag(name = "generalTag")
        self.generalTag.set_property("left-margin-set", True)
        self.generalTag.set_property("left-margin", 10)
        self.generalTag.set_property("family-set", True)
        self.generalTag.set_property("family", "Palatino Linotype")
        self.generalTag.set_property("size-set", True)
        self.generalTag.set_property("size-points", 18)
        self.TagTable.add(self.generalTag)
        
        self.sceneHeader = gtk.TextTag(name = "sceneHeading")
        self.sceneHeader.set_property("underline-set", True)
        self.sceneHeader.set_property("underline", pango.UNDERLINE_SINGLE)
        self.TagTable.add(self.sceneHeader)
        
        self.centered = gtk.TextTag(name = "centered")
        self.centered.set_property("justification-set", True)
        self.centered.set_property("justification", gtk.JUSTIFY_CENTER)
        self.TagTable.add(self.centered)
        
        self.character = gtk.TextTag(name = "characterName")
        self.character.set_property("left-margin-set", True)
        self.character.set_property("left-margin", 200)
        self.TagTable.add(self.character)
        
        self.speech = gtk.TextTag(name = "speech")
        self.speech.set_property("left-margin-set", True)
        self.speech.set_property("left-margin", 100)
        self.speech.set_property("right-margin-set", True)
        self.speech.set_property("right-margin", 100)
        self.TagTable.add(self.speech)
        
        self.action = gtk.TextTag(name = "action")
        self.TagTable.add(self.action)
        
        self.transition = gtk.TextTag(name = "transition")
        self.transition.set_property("justification-set", True)
        self.transition.set_property("justification", gtk.JUSTIFY_RIGHT)
        self.transition.set_property("right-margin-set", True)
        self.transition.set_property("right-margin", 50)
        self.TagTable.add(self.transition)
        
        self.underlined = gtk.TextTag(name = "underlined")
        self.underlined.set_property("underline-set", True)
        self.underlined.set_property("underline", pango.UNDERLINE_SINGLE)
        self.TagTable.add(self.underlined)
        
        self.italic = gtk.TextTag(name = "italic")
        self.italic.set_property("style-set", True)
        self.italic.set_property("style", pango.STYLE_ITALIC)
        self.TagTable.add(self.italic)
        
        self.bold = gtk.TextTag(name = "bold")
        self.bold.set_property("style-set", True)
        self.bold.set_property("style", pango.STYLE_OBLIQUE)
        self.TagTable.add(self.bold)
        
        
        #create the view and its buffer and the scroll-window
        self.TextBuffer = gtk.TextBuffer(table = self.TagTable)
        self.TextBuffer.set_text(self.text)
        self.cursMark = self.TextBuffer.create_mark("cursor", self.TextBuffer.get_start_iter(), True)
        self.TextBuffer.connect("end-user-action", self.textChanged)
        self.TextView = gtk.TextView()
        self.TextView.set_buffer(self.TextBuffer)
        self.TextView.set_property("wrap-mode", gtk.WRAP_WORD)
        
        #create the scroll-window for the textview. Use add for textview and treeview, anything else: use add_with_viewport.
        self.ScrollWindow = gtk.ScrolledWindow()
        self.ScrollWindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.ScrollWindow.add(self.TextView)
        self.scrollAdj = self.ScrollWindow.get_vadjustment()
        self.mainBox.pack_start(self.ScrollWindow, True, True)
        
        #show stuff!
        self.mainBox.show()
        self.optionBox.show()
        self.saveBtn.show()
        self.saveNewBtn.show()
        self.loadBtn.show()
        self.TextView.show()
        self.ScrollWindow.show()
        #show window last, so that if anything takes longer, everything still appears at once
        self.window.show()
    def main(self):
        gtk.main()

if __name__ == "__main__":
    window = Window()
    window.main()
