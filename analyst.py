#/usr/bin/env python

import regex as rx

def analyseText(text, parent = None):
    if parent != None:
        parent.scenes.reset()
    lines = text.split("\n")
    speech = False
    last = False
    a = len(lines)
    for line in lines:
        a-=1
        if a == 0:
            last = True
        speech = analyse(line,parent,speech,last,len(lines)-a-1)





def analyse(line, parent = None, speech = False, last=False, lineNumber=0):
    linetype = "action"
    addtags = []
    if rx.forceAction.match(line):
        speech = False
    elif rx.sceneHeading.match(line):
        linetype = "sceneHeading"
        speech = False
        if parent != None:
            parent.scenes.sceneHeading(line,lineNumber)
    elif rx.transition.match(line):
        linetype = "transition"
        speech = False
    elif rx.characterName.match(line):
        linetype = "characterName"
        speech = True
    elif speech:
        if rx.emptyLine.match(line):
            if rx.emptyCont.match(line):
                linetype = "speech"
            else:
                speech = False
        else:
            linetype = "speech"
    if rx.centered.match(line):
        addtags += ["centered"]
    if rx.underlined.match(line):
        addtags += ["underlined"]
    if rx.italics.match(line):
        addtags += ["italics"]
    
    if parent != None:
        parent.printline(line,linetype,addtags,last)
    
    return speech
        
        


